var ttl_clothes;
var ttl_shoes;
var ttl_accessories;
var ttl_bags;
var q_clothes = document.getElementById('clothes_quantity')
var q_shoes = document.getElementById('shoes_quantity')
var q_accessories = document.getElementById('accessories_quantity')
var q_bags = document.getElementById('bags_quantity')

function notifyMe() {
    console.log("notifyMe() in category.js !!");
    if (!("Notification" in window)) window.alert("Browser does not support desktop notification"); // Check if the browser supports notifications
    else if (Notification.permission === "granted") { // Check whether notification permissions have alredy been granted
        console.log('1-1 !!');
        var img = 'assets/NOTICE.jpg';
        var text = 'Hi there! Welcome to Amazon (category.js1)!!!!';
        var notification = new Notification("category.js 1", { body: text, icon: img }); // If it's okay let's create a notification
        console.log('1-2 !!');
    }
    else if (Notification.permission !== 'denied' || Notification.permission === "default") { // Otherwise, we need to ask the user for permission
        Notification.requestPermission(function (permission) {
            if (permission === "granted") { // If the user accepts, let's create a notification
                console.log('2-1 !!');
                var img = 'assets/NOTICE.jpg';
                var text = 'Hi there! Welcome to Amazon (category.js2)!!!!';
                var notification = new Notification("category.js 2", { body: text, icon: img }); // If it's okay let's create a notification
                console.log('2-2 !!');
            }
        });
    }
}
document.addEventListener('keydown', (e)=>{
    // notifyMe();
    Notification.requestPermission().then(function (result) {
        if (result === 'denied') console.log('Permission wasn\'t granted. Allow a retry.');
        if (result === 'default') console.log('The permission request was dismissed.');
        if (result === 'granted') {
            var img = 'assets/my_image.jpg';
            var text = 'Hi there! Welcome to Amazon !!!!';
            var notification = new Notification("category.js", { body: text, icon: img }); // If it's okay let's create a notification
        }
        // Do something with the granted permission.
    });
})

function init() {
    postsRef = firebase.database().ref('Upload_date/');
    postsRef.once('value').then(function (snapshot) {
        for (var i in snapshot.val()) {
            if( snapshot.val()[i].category == 'Clothes' ) ttl_clothes++;
            else if( snapshot.val()[i].category == 'Shoes' ) ttl_shoes++;
            else if( snapshot.val()[i].category == 'Accessories' ) ttl_accessories++;
            else if( snapshot.val()[i].category == 'Bags' ) ttl_bags++;
        }
        q_clothes.innerHTML = ttl_clothes;
        q_shoes.innerHTML = ttl_shoes;
        q_accessories.innerHTML = ttl_accessories;
        q_bags.innerHTML = ttl_bags;
    });
}

window.onload = function () {
    ttl_clothes = 0;
    ttl_shoes = 0;
    ttl_accessories = 0;
    ttl_bags = 0;
    init();
};