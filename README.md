# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Shopping Site : )
* Key functions (add/delete)
    1. Product Detail page, affording size, color, price, buy, like, product description, seller name, product upload date
    2. shopping pipeline, you need to select color and size before buying, and also a confirmation to make sure you want to buy
    3. user dashboard, which is a place for selling product, and also provided lists of buy/sell record
    4. Explore page(the most popular product voted by every member)
    5. Category Page(see different categories's product's quantity)
    
* Other functions (add/delete)
    1. Location Page
    2. Contact Us Page
    3. Sign in(including signed in by Google account)
    4. Like(voting), you can vote for your favorite product.
    5. Search top, for quick look of what categorities we have.
    6. You are not allowed to buy / vote / look user dashboard unless login first.

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://ssmidtermproject106062142.firebaseapp.com/index.html

# Components Description : 
1. [signin/up] : [可以從網站註冊，也可以使用google登入]
2. [Database read write] : [沒有登入的人只能瀏覽商品，看各個種類有幾種商品；但不能買商品、賣商品、觀看設定介面、為喜歡的商品投票]
3. [RWD] : [能夠在各種螢幕大小下使用]
4. [userpage] : [登入後按右上方Setting可以看到個人的買賣紀錄&賣商品介面，沒有登入會被禁止]
4. [Storage] : [會顯示商品圖片]
5. [探索]: [經過各用戶投票後，會將最受歡迎的商品顯示在此頁]

# Other Functions Description(1~10%) : 
1. [Dropdown] : [商品細節資訊頁面，透過滑鼠點擊動態顯示]
2. [Animation] : [首頁商品將滑鼠移到圖片上會出現商品名稱以及超連結；註冊時會有特效框顯示進度(百分比)]
3. [Locate]: [插入Google Map]
4. [登入按鈕] : [有動畫顯示]

## Security Report (Optional)
1. 沒登入不能買商品、賣商品、觀看設定介面、為喜歡的商品投票
2. 註冊時，會檢查年齡是不是數字、性別是不是'M' or 'F'
3. 信箱會用Regex檢查是否合法
4. 看不到別人的買賣紀錄
