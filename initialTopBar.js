topbar = String.raw`<div class="ui inverted segment menu" style="height: 60px; background - color: #002130; border - radius: 0px; padding - top: 10px">
    <i class="amazon icon" style = "margin-top: auto; margin-bottom: auto"></i>
        <a class="item" id = "TOPbtnSiteName" style="margin-top: auto; margin-bottom: auto">AMAZON</a>
        <div class="ui search">
            <div class="ui icon input">
                <input class="prompt" type="text" placeholder="Search ..." style="width: 200px">
                    <i class="search icon"></i>
                </div>
                <div class="results"></div>
            </div>
            <div class="right menu">
                <div class="item" >
                    <div class="ui red button" id='TOPbtnSetting'>Setting</div>
                </div>
                <div class="item" >
                    <div class="ui red button" id='TOPbtnSignin'>Sign In</div>
                </div>
            </div>
        </div>
        <div class="ui menu" style="color: #08364b; border-radius:0px; margin-top: -15px; ">
            <div class="ui menu secondary pointing seven item" style="height: 50px; margin-top: 0px; padding-bottom: 0.3%">
                <a class="item" id="TOPbtnHome">Home</a>
                <a class="item" id="TOPbtnCategory">Category</a>
                <a class="item" id="TOPbtnExplore">Explore</a>
                <a class="item" id="TOPbtnLocate">Locate</a>
                <a class="item" id="TOPbtnContact">Contact Us</a>
            </div>
        </div>`

// 1. there will be a small white bar at the top of page
// document.body.innerHTML += topbar;
// 2.
// add <div class="" id="123"></div> in the front of body first, and then
t = document.getElementById('TOPBAR');
t.innerHTML += topbar;
// 3. Not working
// tmp = document.body.firstChild;
// document.body.insertBefore(topbar, tmp);
