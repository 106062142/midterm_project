$(document).ready(function () {
    TESTTEST = -999;
    var topsitename = document.getElementById('TOPbtnSiteName');

    var tophome = document.getElementById('TOPbtnHome');
    var topctg = document.getElementById('TOPbtnCategory');
    var topexplore = document.getElementById('TOPbtnExplore');
    var toploc = document.getElementById('TOPbtnLocate');
    var topcontact = document.getElementById('TOPbtnContact');
    var topsetting = document.getElementById('TOPbtnSetting');
    var topsign = document.getElementById('TOPbtnSignin');
    topsitename.addEventListener('click', () => window.location.href = "index.html");
    tophome.addEventListener('click', () => window.location.href = "index.html");
    topctg.addEventListener('click', () => window.location.href = "category.html");
    topexplore.addEventListener('click', () => window.location.href = "explore.html");
    toploc.addEventListener('click', () => window.location.href = "location.html");
    topcontact.addEventListener('click', () => window.location.href = "contactus.html"); 
    topsetting.addEventListener('click', () => window.location.href = "buysellList.html");
    topsign.addEventListener('click', () => window.location.href = "signin.html");
    // var topeng = document.getElementById('TOPbtnEnglish');
    // var topchi = document.getElementById('TOPbtnChinese');
    // topchi.addEventListener('click', ()=>{
    //     tophome.innerHTML = '首頁';
    //     topctg.innerHTML = '種類';
    //     topexplore.innerHTML = '探索';
    //     toploc.innerHTML = '店家位置';
    //     topcontact.innerHTML = '聯絡我們';
    //     topsetting.innerHTML = '設定';
    //     topsign.innerHTML = '登入';
    // })
    // topeng.addEventListener('click', ()=>{
    //     tophome.innerHTML = 'Home';
    //     topctg.innerHTML = 'Category';
    //     topexplore.innerHTML = 'Explore';
    //     toploc.innerHTML = 'Locate';
    //     topcontact.innerHTML = 'Contact Us';
    //     topsetting.innerHTML = 'Setting';
    //     topsign.innerHTML = 'Sign In';
    // })
    document.addEventListener('mousemove', () => {
        var currplace = window.location.href;
        if (currplace.search("index") != -1){
            var active = document.getElementsByClassName('active')[0];
            if (active) active.className = 'item';
            tophome.className += ' active';
        }
        else if (currplace.search("category") != -1){
            var active = document.getElementsByClassName('active')[0];
            if (active) active.className = 'item';
            topctg.className += ' active';
        }
        else if (currplace.search("explore") != -1){
            var active = document.getElementsByClassName('active')[0];
            if (active) active.className = 'item';
            topexplore.className += ' active';
        }
        else if (currplace.search("location") != -1){
            var active = document.getElementsByClassName('active')[0];
            if (active) active.className = 'item';
            toploc.className += ' active';
        }
        else if (currplace.search("contactus") != -1){
            var active = document.getElementsByClassName('active')[0];
            if (active) active.className = 'item';
            topcontact.className += ' active';
        }
    });


    // For dropdown object
    $('.ui.dropdown').dropdown();

    $(".ui.button").click(() => {
        ;
    });

    $('.image').dimmer('hide'); // hide <=> show, initialize

    // For accordion
    $('.ui.accordion').accordion();

    // For (huge star) rating
    $('.rating').rating('disable');
    // $('.rating').rating({ // This will let disable become invalid
    //     initialRating: 2,
    //     maxRating: 4
    // }) // If a metadata rating is specified it will automatically override the default value specified in Javascript.
});

// For dimmer
$('.medium.image').dimmer({
    on: 'hover',
    duration: {
        show: 2500,
        hide: 500
    }
});
$('.ui.dropdown').dropdown({
    on: 'hover',
    // duration: {
    //     show: 2500,
    //     hide: 500
    // }
});
