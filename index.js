var TOTALPRODUCT;
var cate = ['Clothes', 'Shoes', 'Accessories', 'Bags'];
var btnHomePageProductBuy = new Array(20000);
var btnHomePageProductView = new Array(20000);
var time;
// To transfer data through different HTML
// window.name = "Window Name";
// console.log(window.name, "!!!"); // This can also use in other HTML
// Set Item
// sessionStorage.setItem("product_clicked", e.target.id); // OR sessionStorage.product_clicked = e.target.id;
// Get Item
// console.log( sessionStorage.getItem("product_clicked") ); // OR console.log( sessionStorage.product_clicked );

function toAddListener(){
    for(var i = 0; i < TOTALPRODUCT; i++){
        btnHomePageProductView[i] = document.getElementById(`HomeView${i}`);
        btnHomePageProductView[i].addEventListener('click', (e) => {
            sessionStorage.product_clicked = e.target.parentElement.children[0].innerHTML; // parentElement.children[0]: <h2> tag
            window.location.href = "productDetail.html";
        });
    }
}
function AddDimmerObjAnimation() {  // To initialize the new added dimmer object, as same as main.js
    $('.image').dimmer('hide');
    $('.medium.image').dimmer({
        on: 'hover',
        duration: {
            show: 2400,
            hide: 500
        }
    });
}
function AddContentDetail(product_name){
    // console.log(TOTALPRODUCT);
    var str1 = String.raw`<div class="four wide column"> <div class="ui medium image">
                                    <div class="ui dimmer"> <div class="content"> <h2 class="ui inverted header">`;
    var str2 = product_name;
    var str3 = `</h2> <a class="ui inverted pink basic button" id="HomeView${TOTALPRODUCT}">View</a> </div> </div>`;
    var str4 = `<img id="HomeImg${TOTALPRODUCT}" src="/assets/BG_image.png">`; // TODO: image path
    var str5 = `</div> </div>`;
    var ttlstr = str1 + str2 + str3 + str4 + str5;

    var homepage = document.getElementById('homepage');
    homepage.innerHTML += ttlstr;
    TOTALPRODUCT++;
}
// var ImageURL;
// function AddContentImage(img_upload_time) {
//     var storage = firebase.storage();
//     var pathReferenceURL = storage.ref('ProductImage/' + img_upload_time + '.jpg').getDownloadURL().then(function (url) {
//         ImageURL = url;
//     });
//     setTimeout(() => {
//         var x = document.getElementById(`HomeImg${TOTALPRODUCT2}`);
//         x.src = ImageURL;
//         TOTALPRODUCT2++;
//     }, 1500);
// }
function AddContent(cate) {
    return new Promise((resolve, reject) => {
        postsRef = firebase.database().ref('products/' + cate);
        postsRef.once('value').then(function (snapshot) {
            resolve(snapshot.val());
            // for(var j in snapshot.val()) console.log(snapshot.val()[j]); // j is the node index name
        });
    });
}
function init() {
    Promise.all([AddContent('Clothes'), AddContent('Shoes'), AddContent('Accessories'), AddContent('Bags')]).then(values =>{
        // console.log("Succeed");
        // AddContentDetail(values[1]['Addidas shoes 01'].product);
        // AddContentDetail(values[1]['NIKE Jordan 01'].product); // Test
        for(i = 0; i < cate.length; i++){
            for(var j in values[i]){
                // console.log(j, values[i][j]); // j is the node index name

                // time = values[i][j].date; // "2019 4/30, 22:19:54"
                // time = time.replace(' ', ',');
                // time = time.replace('/', ',');
                // time = time.replace(' ', ''); // "2019,4,30,22:19:54"
                if (values[i][j].quantity > 0){
                    AddContentDetail(values[i][j].product); // This will go super fast
                    // AddContentImage(time); // quite slow, so it can use some new things that just created in AddContentDetail()
                }
            }
        }
        AddDimmerObjAnimation();
        toAddListener();
    }).catch(() => {
        console.log("Failed at Promise.all function");
    });
}

window.onload = function () {
    TOTALPRODUCT = 0;
    init();
};