var content = [
    {
        title: 'Clothes',
        description: 'shirt/suit/sweater/coat/jacket...',
    },
    {
        title: 'Shoes',
        description: 'sneakers/sandals/slippers/boots...',
    },
    {
        title: 'Accessories',
        description: 'hat/scarf/necklace/belt...',
    },
    {
        title: 'Bags',
        description: 'backpack/drawstring/purse/briefcase...',
    },
    // etc
];

// For search bar
$('.ui.search').search({
    source: content,
    searchFields: [
        'title'
    ],
    fullTextSearch: false,
    minCharacters: 2,
    // apiSettings: {
    //     url: '//api.github.com/search/repositories?q={query}'
    //     // url: 'http://www.google.com?q={value}'
    //     // url: 'custom-search/?q={query}'
    // },
    // fields: {
    //     results: 'items',
    //     title: 'name',
    //     url: 'html_url'
    // },
});