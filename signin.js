function notifyMe2() {
    console.log("notifyMe2() in signin.js !!");
    if (!("Notification" in window)) window.alert("Browser does not support desktop notification"); // Check if the browser supports notifications
    else if (Notification.permission === "granted") { // Check whether notification permissions have alredy been granted
        var img = 'assets/NOTICE.jpg';
        var text = 'Hi there! Welcome to Amazon (signin.js1)!!!!';
        var notification = new Notification("signin.js 1", { body: text, icon: img }); // If it's okay let's create a notification
        // var notification = new Notification("Hi there! Welcome to Amazon: ) (signin.js 1)");
    }
    else if (Notification.permission !== 'denied' || Notification.permission === "default") { // Otherwise, we need to ask the user for permission
        Notification.requestPermission(function (permission) {
            if (permission === "granted") { // If the user accepts, let's create a notification
                console.log("1. In the requestPermission of signin.js");
                var img = 'assets/NOTICE.jpg';
                var text = 'Hi there! Welcome to Amazon ^_____^ (signin.js 2)!!!!';
                var notification = new Notification('signin.js 2', { body: text, icon: img });
                console.log("2. In the requestPermission of signin.js");
            }
        });
    }
}
function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnGoogle = document.getElementById('btngoogle'); 
    var btnFacebook = document.getElementById('btnfacebook');

    btnSignUp.addEventListener('click', () => window.location.href = "signup.html");

    // ======================================================================================================== //
    // ======================================================================================================== //
    // ======================================================================================================== //
    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field

        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function (result) {
            window.location.href = "buysellList.html";
        }).catch(function (error) {
            // Handle Errors here.
            create_alert("error", error.message);
        });
    });
    document.addEventListener('keydown', function (e) {
        if (e.keyCode == 13 && txtEmail.value != '' && txtPassword.value != ''){
            firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function (result) {
                window.location.href = "buysellList.html";
            }).catch(function (error) {
                // Handle Errors here.
                create_alert("error", error.message);
            });
        } // keyCode == 13 is Enter !!
    });

    // ======================================================================================================== //
    // ======================================================================================================== //
    // ======================================================================================================== //
    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            window.location.href = "buysellList.html";
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredentialtype that was used.
            var credential = error.credential;
        });
        // firebase.auth().signInWithRedirect(provider);
        // firebase.auth().getRedirectResult().then(function (result) {
        //     if (result.credential) {
        //         // This gives you a Google Access Token. You can use it to access the Google API.
        //         var token = result.credential.accessToken;
        //         // ...
        //     }
        //     // The signed-in user info.
        //     var user = result.user;
        // }).catch(function (error) {
        //     // Handle Errors here.
        //     var errorCode = error.code;
        //     var errorMessage = error.message;
        //     // The email of the user's account used.
        //     var email = error.email;
        //     // The firebase.auth.AuthCredentialtype that was used.
        //     var credential = error.credential;
        // });
    });

    // ======================================================================================================== //
    // ======================================================================================================== //
    // ======================================================================================================== //
    btnSignUp.addEventListener('click', function () {
        console.log("This is sign up button !!!!")
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).catch(function (error) {
            // Handle Errors here.
            varerrorCode = error.code;
            varerrorMessage = error.message;
        });
        txtEmail.value = "";
        txtPassword.value = "";
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        // str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        // alertarea.innerHTML = str_html;
    } else if (type == "error") {
        var alr = document.getElementById('foralert');
        alr.removeAttribute("hidden");
        // str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = message;
    }
}

window.onload = function () {
    notifyMe2();
    initApp();
};