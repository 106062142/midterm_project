$(document).ready(() => {
    // For Progress bar (initial here)
    $('#progressBar').progress({
        percent: 0,
    });
});

var inp_firstname   = document.getElementById('firstname')
var inp_lastname    = document.getElementById('lastname')
var inp_age         = document.getElementById('age')
var inp_gender      = document.getElementById('gender')
var inp_postalcode  = document.getElementById('postalcode')
var inp_address     = document.getElementById('address')
var inp_email       = document.getElementById('email')
var inp_password    = document.getElementById('password')
var inp_confirm     = document.getElementById('confirm')
var inp_agreepolicy = document.getElementById('agreepolicy')
var inp_submit      = document.getElementById('submit')

var bar_step = [false, false, false, false, false, false, false, false, false, false];
var name, age, gender, address, email, password, confirm;
var agreepolicy = 0;
inp_agreepolicy.addEventListener('click', () => agreepolicy = (inp_agreepolicy.checked) ? 1 : 0);

inp_submit.addEventListener('click', () => {
    name = `${inp_firstname.value} ${inp_lastname.value}`;
    age = inp_age.value;
    gender = inp_gender.value;
    address = `${inp_postalcode.value}, ${inp_address.value}`;
    email = inp_email.value;
    password = inp_password.value;
    confirm = inp_confirm.value;
    var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

    if (name == '' || name == ' ')                                                                     window.alert("Something wrong in name !!!");
    else if (age == '' || age == ' ' || age < 1 || isNaN(age))                                         window.alert("Something wrong in age!!!");
    else if (gender != 'M' && gender != 'F')                                                           window.alert("Something wrong in gender !!!");
    else if (inp_postalcode.value == '' || inp_postalcode.value == ' ' || isNaN(inp_postalcode.value)) window.alert("Something Wrong in Postal Code !!!");
    else if (inp_address.value == '' || inp_address.value == ' ')                                      window.alert("Something Wrong in address !!!");
    else if (!email.match(emailRule))                                                                   window.alert("Something wrong in email !!!");
    else if (password != confirm || password == '' || password == ' ' || password == '  ')             window.alert("Something wrong in password !!!");
    else if (agreepolicy == 0)                                                                         window.alert("Please check the privacy policy !!!");
    else {
        bar_step = [false, false, false, false, false, false, false, false, false, false];

        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
            varerrorCode = error.code;
            varerrorMessage = error.message;
            create_alert("error", varerrorMessage);
        });


        var idx = 0, tmpstr = '';
        while(1){
            if( email[idx] == '.' ) break;
            else idx++;
        }
        tmpstr = email.substr(0, idx);
        firebase.database().ref('users/' + tmpstr).set({
            name: name,
            age: age,
            gender: gender,
            address: address,
            email: email,
            password: password,
        }, function (err) {
            if (err) console.log("Sign up new user failed !!");
            else console.log("Sign up new user succceed !!");
        });

        setTimeout(() => {
            window.location.href = "signin.html";
        }, 1500); // Necessary to wait, createUserWithEmailAndPassword() won't work properly if leave this page immediately !!
    }
});

// For Progressbar https://semantic-ui.com/modules/progress.html#/usage
window.addEventListener('mousemove', () => ProgreeBarUpdate());
window.addEventListener('keydown', (e) => {
    if( e.keyCode == 9 ) ProgreeBarUpdate(); // keyCode: tab == 9 /  enter == 13
});
function ProgreeBarUpdate(){
    // console.log(bar_step);
    var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
    if (inp_firstname.value != '' && inp_firstname.value != ' ')   bar_step[0] = true; else bar_step[0] = false;
    if (inp_lastname.value != '' && inp_lastname.value != ' ')     bar_step[1] = true; else bar_step[1] = false;
    if (inp_age.value != '' && inp_age.value != ' ')               bar_step[2] = true; else bar_step[2] = false;
    if (inp_gender.value != '')                                    bar_step[3] = true; else bar_step[3] = false;
    if (inp_postalcode.value != '' && inp_postalcode.value != ' ') bar_step[4] = true; else bar_step[4] = false;
    if (inp_address.value != '' && inp_address.value != ' ')       bar_step[5] = true; else bar_step[5] = false;
    if (inp_email.value.match(emailRule))                          bar_step[6] = true; else bar_step[6] = false;
    if (inp_password.value != '' && inp_password.value != ' ')     bar_step[7] = true; else bar_step[7] = false;
    if (inp_confirm.value != '' && inp_confirm.value != ' ')       bar_step[8] = true; else bar_step[8] = false;
    if (agreepolicy == 1)                                          bar_step[9] = true; else bar_step[9] = false;

    var bar_val = 0;
    for(var i = 0; i < 10; i++) if( bar_step[i] == true ) bar_val += 10;
    $('#progressBar').progress({
        percent: bar_val,
    });
     // testBar.progress( {percent: bar_val} ); // ERROR
};

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        // str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        // alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}