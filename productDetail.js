function countDate() {
    var date = new Date;
    var t1 = date.getFullYear();
    var t2 = date.getMonth() + 1;
    var t3 = date.getDate();
    var t5 = date.getHours();
    var t6 = date.getMinutes();
    var t7 = date.getSeconds();
    if (t6 == 0) t6 = '00';
    else if (t6 == 1) t6 = '01'; else if (t6 == 2) t6 = '02'; else if (t6 == 3) t6 = '03';
    else if (t6 == 4) t6 = '04'; else if (t6 == 5) t6 = '05'; else if (t6 == 6) t6 = '06';
    else if (t6 == 7) t6 = '07'; else if (t6 == 8) t6 = '08'; else if (t6 == 9) t6 = '09';
    var uploadDate = + t1 + ' ' + t2 + '/' + t3 + ', ' + t5 + ":" + t6 + ":" + t7;
    return uploadDate;
}

function init() {
    var pd_image = document.getElementById('pd_image'); // ???????????????????????????????????????????????????????????TODO:
    var pd_name = document.getElementById('pd_name');
    var pd_price_quantity = document.getElementById('pd_price');
    var pd_size = document.getElementById('pd_size');
    var pd_color = document.getElementById('pd_color');
    var pd_description = document.getElementById('pd_description');
    var pd_sellername = document.getElementById('pd_sellername');
    var pd_uploaddate = document.getElementById('pd_uploaddate');
    var pd_buy = document.getElementById('pd_buy');
    var pd_like = document.getElementById('pd_like');

    var findProductName = sessionStorage.getItem("product_clicked");
    var SELECT_COLOR = '', SELECT_SIZE = '', FORVOTE, IMAGEURL;
    var p, i, find = false;

    postsRef = firebase.database().ref('Upload_date/');
    postsRef.once('value').then(function (snapshot) {
        for (i in snapshot.val()) { // i is the nodex name in database, and in here(upload_date list) it is a upload date
            if (snapshot.val()[i].product == findProductName && snapshot.val()[i].quantity > 0) {
                find = true;
                FORVOTE = i;
                p = snapshot.val()[i]; // console.log(p, p.product, p.price, p.size, p.color)
                break;
            }
        }
        if (find == false) {
            window.alert("Product have been sold out");
            window.location.href = "index.html";
        }

        // =========================================================================================================
        // Put product image, full version is in getImageUrl() (in explore.js)
        setTimeout(() => {
            var storage = firebase.storage();
            var pathReferenceURL = storage.ref('ProductImage/' + FORVOTE + '.jpg').getDownloadURL().then(function (url) {
                IMAGEURL = url;
            });
            setTimeout(() => {
                pd_image.src = IMAGEURL;
            }, 2100); // To make sure that database give us the correct URL
        }, 2100); // To make sure we get the correct value of FORVOTE

        var sz1 = ['Small', 'Medium', 'Large', 'X-Large', 'Double X-Large'], sz2 = [false, false, false, false, false];
        var sz = 'Size: ', des = '', clr = 'Color: ', tmp;
        // =========================================================================================================
        // Description
        tmp = p.description.split('***');
        for(i = 0; i < tmp.length; i++){
            des += `<li>${tmp[i]}</li>`;
        }
        // =========================================================================================================
        // Size
        tmp = p.size.split(',');
        for (i = 0; i < tmp.length; i ++) {
            if (tmp[i] == 'S') sz2[0] = true;
            else if (tmp[i] == 'M') sz2[1] = true;
            else if (tmp[i] == 'L') sz2[2] = true;
            else if (tmp[i] == 'XL') sz2[3] = true;
            else if (tmp[i] == 'XXL') sz2[4] = true;
        }
        for (i = 0; i < 5; i++) {
            if (sz2[i] == true) sz += `<button class="ui label" id = "pd_size_${sz1[i]}" style = "color:#D2E0E9;\
            background-color: #4579A0">${sz1[i]}</button>\n`
        }
        // =========================================================================================================
        // Color
        tmp = p.color.split('&');
        for (i = 0; i < tmp.length; i++) {
            var t = tmp[i].slice(1, -1);
            clr += `<button class="ui circular label" id="pd_color_${t}" style="border-width: 0.1em; border-color: #002130;\
            background-color: rgb${tmp[i]}"></button>`;
        }
        // =========================================================================================================
        // Place innerHTML
        pd_name.innerHTML = p.product;
        pd_price_quantity.innerHTML = `NTD $${p.price} (<span style="color: red"> ${p.quantity}</span> available )`;
        pd_size.innerHTML = sz;
        pd_color.innerHTML = clr;
        pd_description.innerHTML = des;
        pd_sellername.innerHTML = p.seller; // p.seller = XXX@ZZZ.com
        pd_uploaddate.innerHTML = p.date;
        // =========================================================================================================
        // Add Event Listener
        var sizeListener = new Array(5), idx = 0;
        for (var k = 0; k < 5; k++) {
            if (sz2[k] == true){
                sizeListener[idx] = document.getElementById(`pd_size_${sz1[k]}`);

                if(k == 0) sizeListener[idx].addEventListener('click', () => SELECT_SIZE = 'Small');
                else if(k == 1) sizeListener[idx].addEventListener('click', () => SELECT_SIZE = 'Medium');
                else if(k == 2) sizeListener[idx].addEventListener('click', () => SELECT_SIZE = 'Large');
                else if (k == 3) sizeListener[idx].addEventListener('click', () => SELECT_SIZE = 'X-Large');
                else if (k == 4) sizeListener[idx].addEventListener('click', () => SELECT_SIZE = 'Double X-Large');
                // sizeListener[idx].addEventListener('click', () => SELECT_SIZE = sz1[k]); // ??????????????????????????????????TODO:
                idx++;
            }
        }
        var colorListener = new Array(15);
        tmp = new Array(15);
        tmpcolor = p.color.split('&');
        idx = 0;
        for (i = 0; i < tmpcolor.length; i++) {
            var t = tmpcolor[i].slice(1, -1);
            tmp[i] = t;
        }
        for (var k = 0; k < tmpcolor.length; k++) {
            var t = tmpcolor[k].slice(1, -1);
            colorListener[idx] = document.getElementById(`pd_color_${t}`);

            if(k == 0) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[0]})`);
            else if(k == 1) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[1]})`);
            else if(k == 2) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[2]})`);
            else if(k == 3) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[3]})`);
            else if(k == 4) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[4]})`);
            else if(k == 5) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[5]})`);
            else if(k == 6) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[6]})`);
            else if(k == 7) colorListener[idx].addEventListener('click', () => SELECT_COLOR = `RGB(${tmp[7]})`);
            idx++;
        }

        // =========================================================================================================
        // Buy Event
        pd_buy.addEventListener('click', () => {
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    if( p.quantity < 1 ){ // Check if the product had sold out before starting transaction
                        window.alert("Product have been sold out");
                        window.location.href = "index.html";
                    }
                    if (SELECT_SIZE == '' || SELECT_COLOR == '') window.alert('Please select color and size !!!');
                    else{
                        var r = confirm(`Notice!! Are you sure to bought this product?\n`+
                                            `Product name: ${p.product}\nProduct size:${SELECT_SIZE}\nProduct color: ${SELECT_COLOR}`);
                        if (r == true) {
                            window.alert("Thanks for buying !!");

                            // var Original_Quantity;
                            // UpdateQuantity = firebase.database().ref('products/' + `${p.category}`);
                            // UpdateQuantity.once('value').then(function (snapshot) {
                            //     for (i in snapshot.val()){
                            //         // console.log("1", p.product, "1");
                            //         // console.log("2", i, "2");
                            //         // console.log("3", snapshot.val()[i].product, "3");
                            //         // snapshot.val()[i].product == i ??????????????????????????????????????
                            //     }
                            // });
                            // ==========================================================================================================
                            // Update the quantity in database
                            // ==============================
                            // 1. User, and also add a buy record in this user's data
                            var buyerEmail = user.email, buyDate = countDate(); // user.email is the buyer's email !!!!!
                            var sellerEmail = p.seller;
                            idx = 0;
                            while (1) {
                                if (buyerEmail[idx] == '.') break;
                                else idx++;
                            }
                            buyerEmail = buyerEmail.substr(0, idx); // 123@gmail.com -> 123@gmail
                            idx = 0;
                            while (1) {
                                if (sellerEmail[idx] == '.') break;
                                else idx++;
                            }
                            sellerEmail = sellerEmail.substr(0, idx); // 123@gmail.com -> 123@gmail
                            
                            firebase.database().ref(`users/${sellerEmail}/commodity`).once('value').then(function (snapshot) {
                                for (i in snapshot.val()) {
                                    // console.log(i); // i.product is illegal because i is a random number (we use push() instead of set() advance)
                                    if (snapshot.val()[i].product == p.product ){
                                        // Update the quantity in seller's data
                                        firebase.database().ref('users/' + `${sellerEmail}/commodity/${i}/quantity`).set(p.quantity - 1);
                                        // Add a new transaction record in buyer's data
                                        firebase.database().ref('users/' + `${buyerEmail}/buy`).push({
                                            product: p.product,
                                            category: p.category,
                                            price: p.price,
                                            color: SELECT_COLOR,
                                            size: SELECT_SIZE,
                                            seller: p.seller, // p.seller: xxx@aaa.com / sellerEmail: xxx@aaa
                                            buy_date: buyDate,
                                        });
                                    }
                                }
                            });
                            // ==============================
                            // 2. Upload date
                            var time = p.date; // "2019 4/30, 22:19:54"
                            time = time.replace(' ', ',');
                            time = time.replace('/', ',');
                            time = time.replace(' ', ''); // "2019,4,30,22:19:54"
                            firebase.database().ref('Upload_date/' + `${time}/quantity`).set(p.quantity - 1);
                            // ==============================
                            // 3. Product
                            firebase.database().ref('products/' + `${p.category}/${p.product}/quantity`).set(p.quantity - 1);
                            setTimeout(() => {
                                p.quantity -= 1; // Cause User part take some time, so we need to wait for it
                                window.location.href = 'buysellList.html';
                            }, 1000);
                        } else {
                            window.alert("Deal has been canceled !!");
                        }
                    }
                } else{
                    window.alert("Please sign in to buy !!!");
                }
            });
        });
        pd_like.addEventListener('click', () => {
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) { // Check user login
                    postsRef = firebase.database().ref('vote/'+ `${FORVOTE}`);
                    postsRef.once('value').then(function (snapshot) {
                        // Because once function takes some time, so we renew the value inside
                        var num_of_votes = snapshot.val()['supporting'];
                        firebase.database().ref('vote/' + `${FORVOTE}`).set({supporting: num_of_votes + 1});
                        // console.log(snapshot.val(), snapshot.val()['supporting']); // {supporting: xx} xx
                    });
                    window.alert("Thanks for your appreciation !!");
                } else {
                    window.alert('You should log in to vote for your favorite product');
                }
            });
        });
    }); // end of once
}

window.onload = function () {
    init();
};