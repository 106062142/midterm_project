var tosell_name        = document.getElementById('productname');
var tosell_category    = document.getElementById('product_category');
var tosell_price       = document.getElementById('productprice');
var tosell_quantity    = document.getElementById('productquantity');
var tosell_color       = document.getElementById('productcolor');
var tosell_description = document.getElementById('productdescription');
var tosell_productbtn  = document.getElementById('productbtn');
var cate = '';
var FIRSTTIME;
var UPLOADIMAGE;
// document.addEventListener('click', ()=>{ // It's a search selection!!!!!!!!!!!!!!!!!!!!!!!!
//     var x = document.getElementById('product_category');
//     console.log(x.innerHTML);
//     console.log($('#selectsize').val(), $('#selectsize').val()[0], "!!!!" ); // class="ui fluid search dropdown"
//     if ( $('#selectsize').val() == '' )  console.log( "1111" ); // it returns an array, but need to compare with '' instead of []
//     if ( $('#selectsize').val()[0] == 'M' )  console.log( "2222" );
//     console.log($('#selectsize').val().join()); // OR .toString()
// });

// firebase.database().ref('products/shoes/Newest NIKE Jordan/color').set('12345'); // update !!!!!!!!!!!!!!!!!!!
// // =================================================================
// // ver.1
// firebase.auth().onAuthStateChanged(function (user) {
// // Unlike firebase.auth().currentUser, this function will wait and return when it terminates and get the value.
// // So there will be no 'null' type return simply because firebase took a long time to deal with
//     if (user) {
//     }
// });
// // =================================================================
// // ver.2
// var user = firebase.auth().currentUser;
// if (user) {
//     // User is signed in.
//     if (user != null) {
//         console.log(user.displayName);
//         console.log(user.email);
//         console.log(user.photoURL);
//         console.log(user.emailVerified);
//         console.log(user.uid);
//     }
// }

// document.addEventListener('keydown', (e)=>{
//     var z = $('#selectsize').val();
//     console.log(z);
//     if(e.keyCode == 13){
//         var a = document.getElementsByClassName('ui label transition visible'); // HTMLCollection(X), X is how many tags be selected (Dynamic added)
//         // console.log(a[0]); // No.1 tag been selected
//         for(var t = 0; t < 5; t++){
//             if(a[0]){ // always [0] !!!!
//                 console.log(a[0]); // <a class="ui label transition visible" data-value="S" style="display: inline-block !important;">S<i class="delete icon"></i></a>
//                 var p = document.getElementsByClassName('ui fluid search dropdown selection multiple')[0];
//                 console.log(p);
//                 p.removeChild(a[0]);
//             }
//         }
//         $('#selectsize').val(""); // This should go last
//     }
// });
// ===================================================================================================================
// ===================================================================================================================
// TODO: Add photo
function handleFileSelect(evt) {
    UPLOADIMAGE = evt.target.files; // FileList object
    // files is a FileList of File objects. List some properties.
    var output = [];
    var storageRef = firebase.storage().ref('ProductImage/');
    var testRef = storageRef.child("innerFolder/1234.jpg");
    for (var i = 0, f; f = UPLOADIMAGE[i]; i++) {
        output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
            f.size, ' bytes, last modified: ',
            f.lastModifiedDate.toLocaleDateString(), '</li>');
    }
    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('UPLOADPHOTO').addEventListener('change', handleFileSelect, false);

// Test of upload image
// document.addEventListener('keydown', ()=>{
//     var storageRef = firebase.storage().ref('ProductImage/');
//     var testRef = storageRef.child("aaaa.jpg");
//     for (var i = 0, f; f = UPLOADIMAGE[i]; i++) {
//         testRef.put(UPLOADIMAGE[i]).then(function (snapshot) {
//                 console.log(snapshot);
//                 console.log(snapshot.ref);
//                 console.log(snapshot.ref.getDownloadURL());
//                 snapshot.ref.getDownloadURL().then(function(dl_url){
//                     console.log(dl_url); // The url of the upload image!!!!
//                 })
//         });
//     }
// });
// ===================================================================================================================
// ===================================================================================================================
var currUserEmail;
var postsRef;
function findCurrentUser(){
    var user = firebase.auth().currentUser;
    var tmp = user.email;
    var idx = 0, finduser = '';

    while (1) {
        if (tmp[idx] == '.') break;
        else idx++;
    }
    finduser = tmp.substr(0, idx);
    return finduser;
}

function countDate() {
    var date = new Date;
    var t1 = date.getFullYear();
    var t2 = date.getMonth() + 1;
    var t3 = date.getDate();
    var t5 = date.getHours();
    var t6 = date.getMinutes();
    var t7 = date.getSeconds();
    if (t6 == 0) t6 = '00';
    else if (t6 == 1) t6 = '01'; else if (t6 == 2) t6 = '02'; else if (t6 == 3) t6 = '03';
    else if (t6 == 4) t6 = '04'; else if (t6 == 5) t6 = '05'; else if (t6 == 6) t6 = '06';
    else if (t6 == 7) t6 = '07'; else if (t6 == 8) t6 = '08'; else if (t6 == 9) t6 = '09';
    var uploadDate = + t1 + ' ' + t2 + '/' + t3 + ', ' + t5 + ":" + t6 + ":" + t7;
    var forFirebaseDate = t1 + ',' + t2 + ',' + t3 + ',' + t5 + ':' + t6 + ":" + t7;
    return [uploadDate, forFirebaseDate];
}

function init() {
    // var test  = document.getElementById('Select'); // DivElement
    // test.addEventListener('click', ()=>{
    //     var cc = test.getElementsByClassName('item');
    //     console.log(cc); // Collection Obj
    //     // console.log(cc.length);
    //     console.log(cc.item(0).innerHTML);
    //     // console.log(cc[0]);
    // })

    firebase.auth().onAuthStateChanged(function (user) {
        var pageNotSignIn = document.getElementById('prohibit_buysellList');
        var pageSignIn = document.getElementById('allow_buysellList');
        // Check user login
        if (user) {
            // ======================================================================================================================
            // Select display block
            pageNotSignIn.setAttribute("hidden", "");
            pageSignIn.removeAttribute("hidden");
            
            // =====================================================================================================================
            // Logout Event
            var toLogout = document.getElementById('logoutbtn');
            toLogout.addEventListener("click", () => {
                firebase.auth().signOut().then(function (error) {
                    // The error is undefined !!
                    create_alert("Signout Success");
                    window.location.href = "signin.html";
                }).catch(function (error) {
                    window.alert(error);
                    create_alert("Signout Error", error.message);
                })
            });
        } else{
            // Not log in yet;
            pageNotSignIn.removeAttribute("hidden");
            pageSignIn.setAttribute("hidden", "");
        }
    }); // end of onAuthStateChanged



    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var tmp = user.email;
            var idx = 0;

            while (1) {
                if (tmp[idx] == '.') break;
                else idx++;
            }
            tmp = tmp.substr(0, idx); // 123@gmail.com -> 123@gmail

            // =====================================================================================================================
            // Show purchase list
            postsRef = firebase.database().ref('users/' + tmp + '/buy');
            postsRef.once('value').then(function (snapshot) {
                for (var i in snapshot.val()) {
                    var newContent = document.createElement('tr');

                    var s1 = '<td>' + snapshot.val()[i].buy_date + '</td>';
                    var s2 = '<td>' + snapshot.val()[i].product + '</td>';
                    var s3 = '<td>' + snapshot.val()[i].category + '</td>';
                    var s4 = '<td>' + '$' + snapshot.val()[i].price + '</td>';
                    var s5 = '<td>' + snapshot.val()[i].color + '</td>';
                    var s6 = '<td>' + snapshot.val()[i].size + '</td>';
                    var s7 = '<td>' + snapshot.val()[i].seller + '</td>';

                    newContent.innerHTML = s1 + s2 + s3 + s4 + s5 + s6 + s7;
                    document.getElementsByTagName('tbody')[0].appendChild(newContent);
                }
                // Purshase list doesn;t need on()
                // postsRef.on('value', function (snapshot) { // this will go wrong when sold out list updated( this on() will be called too )
            });
            // =====================================================================================================================
            // Show sold out list
            postsRef = firebase.database().ref('users/' + tmp + '/commodity');
            // postsRef.once('value').then(function (snapshot) {
            firebase.database().ref('users/' + tmp + '/commodity').on('value', function (snapshot) {
                document.getElementsByTagName('tbody')[1].innerHTML = ''; // Clear it first
                for (var i in snapshot.val()) {
                    FIRSTTIME = true;
                    var newContent = document.createElement('tr');

                    var s1 = '<td>' + snapshot.val()[i].date + '</td>';
                    var s2 = '<td>' + snapshot.val()[i].product + '</td>';
                    var s3 = '<td>' + snapshot.val()[i].category + '</td>';
                    var s4 = '<td>' + '$' + snapshot.val()[i].price + '</td>';
                    var s5 = '<td>' + snapshot.val()[i].quantity + '</td>';
                    var s6 = '<td>' + snapshot.val()[i].color + '</td>';
                    var s7 = '<td>' + snapshot.val()[i].size + '</td>';
                    var s8 = '<td>' + snapshot.val()[i].description.slice(0, 20) + ' .....</td>';
                    
                    newContent.innerHTML = s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8;
                    document.getElementsByTagName('tbody')[1].appendChild(newContent);
                }
                // firebase.database().ref('users/' + tmp + '/commodity').on('value', function (snapshot) {
                //     if (FIRSTTIME == false) for (var i in snapshot.val()) {
                //         var newContent = document.createElement('tr');

                //         var s1 = '<td>' + snapshot.val()[i].date + '</td>';
                //         var s2 = '<td>' + snapshot.val()[i].product + '</td>';
                //         var s3 = '<td>' + snapshot.val()[i].category + '</td>';
                //         var s4 = '<td>' + '$' + snapshot.val()[i].price + '</td>';
                //         var s5 = '<td>' + snapshot.val()[i].quantity + '</td>';
                //         var s6 = '<td>' + snapshot.val()[i].color + '</td>';
                //         var s7 = '<td>' + snapshot.val()[i].size + '</td>';
                //         var s8 = '<td>' + snapshot.val()[i].description + '</td>';
                        
                //         newContent.innerHTML = s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8;
                //         document.getElementsByTagName('tbody')[1].appendChild(newContent);
                //     }
                //     else FIRSTTIME = false;
                // });
            });
        } else{
            console.log("Not login yet (Read user purchase/sold out list faild)");
        }
    });

    // =====================================================================================================================
    // =====================================================================================================================
    // =====================================================================================================================
    // TODO: Sell Product
    tosell_productbtn.addEventListener('click', () => {
        var clr_flag = 0;
        tosell_color.value = tosell_color.value.replace(/ /g, ""); // Remove all space
        for(i = 0; i < tosell_color.value; i++){
            if ( !isNaN(tosell_color.value[i]) || tosell_color.value[i] == ',' || tosell_color.value[i] == '&' ){
                clr_flag = 1;
            }
            if(i > 0){
                if (tosell_color.value[i] == '&' && tosell_color.value[i - 1] == '&') clr_flag = 1;
            }
        }
        var tmp = tosell_color.value.split('&');  // tmp: ["(c1, c2, c3)", "(c4, c5, c6)", ....]
        for (i = 0; i < tmp.length; i++) {
            if (tmp[i][0] != '(' && tmp[i][ tmp[i].length - 1 ] != ')'){
                clr_flag = 1;
                break;
            }
            var tmp2 = tmp[i].slice(1, -1).split(',');
            if(tmp2.length != 3 || tmp2[0] < 0 || tmp2[1] < 0 || tmp2[2] < 0 || tmp2[0] > 255 || tmp2[1] > 255 || tmp2[2] > 255 ){
                clr_flag = 1;
                break;
            }
        }
        if (clr_flag == 1)                                                      window.alert('Illegal color !!!');
        else if ( tosell_name.value == '' || tosell_name.value == ' ' )         window.alert('Illegal Name !!');
        else if ( tosell_category.innerHTML == 'Select Category' )              window.alert('Select Category !!');
        else if ( $('#selectsize').val() == '' )                                window.alert('Select Size !!');
        else if ( isNaN(tosell_price.value) || tosell_price.value < 1 )         window.alert('Illegal Price !!');
        else if ( isNaN(tosell_quantity.value) || tosell_quantity.value < 1  )  window.alert('Illegal quntity !!');
        else if ( tosell_color.value == '' || tosell_color.value == ' ' )       window.alert('Illegal color !!');
        else if ( tosell_description.value == '' || tosell_description == ' ' ) window.alert('Please describe your product !!');
        else{
            var [recdate, recdate_forfirebase] = countDate(); // [] not {}
            var productImageUrl = '';

            finduser = findCurrentUser(); // Return the user's ID on firebase console
            // =========================================================================================
            // Add product image to storage !!!!!!!!!!!!!!!!!!!!!!
            var storageRef = firebase.storage().ref('ProductImage/');
            var testRef = storageRef.child(recdate_forfirebase + ".jpg");
            for (var i = 0, f; f = UPLOADIMAGE[i]; i++) {
                testRef.put(UPLOADIMAGE[i]).then(function (snapshot) {
                    console.log(snapshot);
                    console.log(snapshot.ref);
                    console.log(snapshot.ref.getDownloadURL());
                    snapshot.ref.getDownloadURL().then(function (dl_url) {
                        // console.log(dl_url);
                        productImageUrl = dl_url;
                    })
                });
            }

            // =========================================================================================
            // 1. Add product to product list, sort by category
            // console.log(tosell_category, `${tosell_category}/${tosell_name.value}`);
            firebase.database().ref('products/' + `${tosell_category.innerHTML}/${tosell_name.value}`).set({
                seller: finduser + ".com",
                date: recdate,
                product: tosell_name.value,
                category: tosell_category.innerHTML,
                size: $('#selectsize').val().join(), // OR $('#selectsize').val().toString()
                price: tosell_price.value,
                quantity: tosell_quantity.value,
                color: tosell_color.value,
                description: tosell_description.value,
            }, (err) => {
                if(err) console.log("New product update failed !!");
                // else console.log("New product update succeed !!!")
            });
            // =========================================================================================
            // 2. Add product to Upload list, sort by upload date
            firebase.database().ref('Upload_date/' + recdate_forfirebase).set({
                seller: finduser + ".com",
                date: recdate,
                product: tosell_name.value,
                category: tosell_category.innerHTML,
                size: $('#selectsize').val().join(),
                price: tosell_price.value,
                quantity: tosell_quantity.value,
                color: tosell_color.value,
                description: tosell_description.value,
            }, (err) => {
                if(err) console.log("New product update by date failed !!");
                // else console.log("New product update by date succeed !!!")
            });
            // =========================================================================================
            // 3. Add product to personal sell record
            finduser = findCurrentUser(); // Return the user's ID on firebase console
            firebase.database().ref('users/' + finduser + "/commodity").push({
                date: recdate,
                product: tosell_name.value,
                category: tosell_category.innerHTML,
                size: $('#selectsize').val().join(),
                price: tosell_price.value,
                quantity: tosell_quantity.value,
                color: tosell_color.value,
                description: tosell_description.value,
            }, (err) => {
                if(err) console.log("New product update failed !!");
                // else console.log("New product update succeed !!!")
            });

            // =========================================================================================
            // 4. Add a vote
            firebase.database().ref('vote/' + recdate_forfirebase).set({
                supporting: 0,
            }, (err) => {
                if (err) console.log("New product update failed (on vote) !!");
                // else console.log("New product update succeed !!!")
            });
            // =========================================================================================
            // 5. Clear
            tosell_name.value = '';
            tosell_category.innerHTML = 'Select Category'; // This is default text
            tosell_price.value = '';
            tosell_quantity.value = '';
            tosell_color.value = '';
            tosell_description.value = '';

            // var a = document.getElementsByClassName('ui label transition visible'); // HTMLCollection(X), X is how many tags be selected (Dynamic added)
            // for (var t = 0; t < 5; t++) {
            //     if (a[t]) { // always [0] !!!!
            //         a[t].className = 'ui label transition'; // remove 'visible'
            //     }
            // }
            // for (var t = 0; t < 5; t++) {
            //     if (a[0]) { // always [0] !!!!
            //         var p = document.getElementsByClassName('ui fluid search dropdown selection multiple')[0];
            //         p.removeChild(a[0]);
            //     }
            // }
            // $('#selectsize').val(""); // OR $('#selectsize').val(null) OR $('#selectsize').prop('selectedIndex', 0): this works but not sure
            window.alert("Done.");
            window.alert("Your product will be update very soon.");
            setTimeout(() => {
                window.location.href = '';
            }, 2600);
        }
    });

    // Custom alert
    function create_alert(type, message) {
        var alertarea = document.getElementById('custom-alert');
        if (type == "success") {
            // str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            // alertarea.innerHTML = str_html;
        } else if (type == "error") {
            var alr = document.getElementById('foralert');
            alr.removeAttribute("hidden");
            // str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = message;
        }
    }
} // end of init()

window.onload = function () {
    FIRSTTIME = false;
    init();
};